#!/usr/bin/env python
# coding: utf-8

"""
Python-ping
"""

__title__ = 'python-ping'
__version__ = '0.1'
__author__ = 'julien.hautefeuille@inserm.fr'
__licence__ = 'BSD'

from threading import Thread
import subprocess
from queue import Queue

num_threads = 32
queue = Queue()
ips = ["127.0.0.1", "8.8.8.8", "192.168.0.23"]

def pinger(i, q):
    while True:
        ip = q.get()
        ret = subprocess.call("ping -w 1 -c 1 %s" % ip,
            shell=True,
            stdout=open('/dev/null', 'w'),
            stderr=subprocess.STDOUT)
        if ret == 0:
            print("%s => est en vie" % ip)
        else:
            print("%s => ne repond pas" % ip)
        q.task_done()

for i in range(num_threads):
    worker = Thread(target=pinger, args=(i, queue))
    worker.setDaemon(True)
    worker.start()

for ip in ips:
    queue.put(ip)

queue.join()
