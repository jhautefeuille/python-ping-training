#!/usr/bin/env python
# coding: utf-8

"""

    Python-asyncio-ping


"""

__title__ = 'python-asyncio-ping'
__version__ = '0.2'
__author__ = 'julien.hautefeuille@inserm.fr'
__licence__ = 'BSD'

import asyncio
import shlex
from functools import wraps
import ipaddress


q = asyncio.Queue(maxsize=0)

#ips = ["127.0.0.1", "8.8.8.8", "192.168.0.23"]

ips = list(ipaddress.ip_network('10.59.6.0/25').hosts())


def ban_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print("----8<-------------------------------------------------------")
        func(*args, **kwargs)
        print("----8<-------------------------------------------------------")
    return wrapper

@ban_decorator
def motd():
    print("\t Need a medic now !")

@asyncio.coroutine
def producer():
    for ip in ips:
        ping_cmd = "ping -w 1 -c 1 {}".format(ip)
        p = yield from asyncio.create_subprocess_exec(* shlex.split(ping_cmd),
            stdout=open('/dev/null', 'w'),
            stderr=asyncio.subprocess.STDOUT)
        print("Test IP : {}".format(ip))
        _outs, _err = yield from p.communicate()
        if p.returncode:
            yield from q.put("DEAD : %s" % ip)
        else:
            yield from q.put("LIVE : %s" % ip)

@asyncio.coroutine
def consumer():
    while not q.empty():
        elems = yield from q.get()
        print(elems)
    asyncio.get_event_loop().stop()

if __name__ == "__main__":
    motd()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(producer())
    loop.run_until_complete(consumer())
    loop.close()

